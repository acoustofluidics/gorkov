# distribution package
[build-system]
requires = ["setuptools", "setuptools-scm"]
build-backend = "setuptools.build_meta"

[project]
name = "osaft"
description = "An Open-Source Python Library For Acoustofluidics"
readme = "README.md"
requires-python = ">=3.9"
keywords = [
    "msaf",
    "arf",
    "scattering",
    "acoustofluidic",
    "acoustic-radiation-force"]
authors = [
    {name = "gorkovpy developer team", email="gorkovpy@gmail.com"},
]
license = {text = "LPGL v3"}
classifiers = [
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Physics",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
]
dependencies = [
    "numpy>=1.22",
    "scipy>=1.8",
    "sympy>=1.10",
    "matplotlib>=3.5.1",
    "mpmath>=1.2.0",
]
dynamic = ["version"]

[project.optional-dependencies]
dev = [
    "pre-commit",
    "flake8-print",
    "black",
    "pytest",
    "pytest-cov",
    "pytest-xdist",
    "twine",
    "build"]
docs = [
    "sphinx>=5.0",
    "sphinx-autodoc-typehints",
    "sphinx-rtd-theme",
    "sphinx-copybutton",
    "sphinx-gallery>=0.11.0",
    "memory-profiler",
    "furo",
    "autoclasstoc",
    "sphinx_autodoc_defaultargs"]

[project.urls]
Documentation = "https://osaft.readthedocs.io/en/latest/"
Repository = "https://gitlab.com/acoustofluidics/osaft"
Tracker = "https://gitlab.com/acoustofluidics/osaft/-/issues"

[tool.setuptools.dynamic]
version = {"attr" = "setuptools_scm.get_version"}

[tool.setuptools_scm]
write_to = "osaft/core/version.py"
tag_regex = "^v(\\d+\\.\\d+\\.\\d+)$"

[tool.add-trailing-comma]
exclude = 'osaft/tests/doinikov2021viscous/test_streaming.py'

[tool.black]
line-length = 79
target-version = ['py39','py310','py311']
force-exclude = 'create_changelog.py'

[tool.pytest.ini_options]
addopts = "--cov=osaft --junitxml=report.xml"
testpaths = ["osaft/tests",]

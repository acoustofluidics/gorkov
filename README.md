# osaft

[![Coverage][cov-image]][cov-url]
[![PyPi][pypi-image]][pypi-url]
[![License][lic-image]][lic-url]
[![Pipeline][pipeline-image]][pipeline-url]
[![Docs][docs-image]][docs-url]
[![Supported][python-image]][python-url]

The OSAFT Python package implements classical theories for the
computation of the acoustic scattering, acoustic streaming, and the acoustic
radiation force on a particle subjected to an ultrasonic wave. The package
provides a unified API that allows for the simple evaluation of quantities
often used in the research field of microscale acoustofluidics.
Also included in the package are plotting tools (using Matplotlib), so
generated results can easily be illustrated.

## Installation

All systems with `pip`/`python` installed

```sh
python -m pip install osaft
```

If you want to learn more go to the documentation's
[installation page](https://osaft.readthedocs.io/en/stable/installation.html).

## Usage example

Examples how the OSAFT library can be used can be found on our
[examples page](https://osaft.readthedocs.io/en/stable/examples) of the
documentation.

## Publication

A [research article](https://www.frontiersin.org/articles/10.3389/fphy.2022.893686/full)
(open-access) on the OSAFT library has been published the Frontiers in Physics.

## Release History

Find the changes introduced with new releases in the
[``CHANGELOG.md``](https://gitlab.com/acoustofluidics/osaft/-/blob/developer/CHANGELOG.md).

## License
The distribution is licenced under the GNU Lesser General Public License. See
[``LICENSE.md``](https://gitlab.com/acoustofluidics/osaft/-/blob/developer/LICENSE.md)
to learn more.

## Contributing

1. Fork it (<https://gitlab.com/acoustofluidics/osaft/-/forks/new>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[pipeline-image]: https://gitlab.com/acoustofluidics/osaft/badges/developer/pipeline.svg
[pipeline-url]: https://gitlab.com/acoustofluidics/osaft/-/pipelines/
[pypi-image]: https://img.shields.io/pypi/v/osaft?color=ee7029
[pypi-url]: https://gitlab.com/acoustofluidics/osaft/-/tags
[cov-image]: https://gitlab.com/acoustofluidics/osaft/badges/developer/coverage.svg
[cov-url]: https://gitlab.com/acoustofluidics/osaft/badges/developer/coverage.svg
[lic-image]: https://img.shields.io/badge/License-LGPL%20v3-gold.svg
[lic-url]: https://www.gnu.org/licenses/lgpl-3.0
[docs-image]: https://readthedocs.org/projects/osaft/badge/?version=stable
[docs-url]: https://osaft.readthedocs.io/en/stable/
[python-image]: https://img.shields.io/pypi/pyversions/osaft
[python-url]: https://www.python.org

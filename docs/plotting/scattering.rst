Scattering Plots
=====================

.. currentmodule:: osaft.plotting.scattering

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~fluid_plots.FluidScatteringPlot
   ~particle_plots.ParticleScatteringPlot
   ~particle_plots.ParticleWireframePlot

.. _Doinikov2021Viscous:

Doinikov (viscous fluid - elastic sphere; 2021)
================================================


`Link to paper (acoustic streaming)
<https://journals.aps.org/pre/abstract/10.1103/PhysRevE.104.065107>`_

`Link to paper (acoustic radiation force)
<https://journals.aps.org/pre/abstract/10.1103/PhysRevE.104.065108>`_

.. inheritance-diagram::
    osaft.solutions.doinikov2021viscous.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.doinikov2021viscous

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~scattering.ScatteringField
   ~streaming.StreamingField
   ~arf.ARF

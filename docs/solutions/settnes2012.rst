.. _Settnes2012:

Settnes & Bruus (2012)
=======================

`Link to paper
<https://journals.aps.org/pre/pdf/10.1103/PhysRevE.85.016327>`_

.. inheritance-diagram::
    osaft.solutions.settnes2012.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.settnes2012

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~arf.ARF

.. _Doinikov1994Base:

Doinikov 1994 Base Solution
=============================

This is not an actual solution but only provides base classes for
:ref:`Doinikov1994Rigid` and :ref:`Doinikov1994Compressible`.


.. currentmodule:: osaft.solutions.basedoinikov1994

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~base.BaseDoinikov1994
   ~scattering.BaseScatteringDoinikov1994

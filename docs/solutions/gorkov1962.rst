.. _Gorkov1962:

Gor'kov (1962)
====================

We were unable to find a version of the article "On forces acting on a small
particle in an acoustical field in an ideal fluid" online, on which this
solution is based. We therefore provide a link to an article by Henrik Bruus
where he explains Gor'kov's theory.

`Link to paper
<https://pubs.rsc.org/en/content/articlehtml/2012/lc/c2lc21068a>`_

.. inheritance-diagram::
    osaft.solutions.gorkov1962.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.gorkov1962

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~arf.ARF
